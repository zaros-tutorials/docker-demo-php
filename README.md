# My Custom PHP for Lumen

This repository hosts the Dockerfiles to build the PHP container 
used to support Lumen-based applications.

## Building the image 

```$xslt
$ docker build -f Dockerfile-php-prod -t demo-php:latest .
$ docker tag demo-php {username}/demo-php
$ docker push {username}/demo-php
```

The development image

```$xslt
$ docker build -f Dockerfile-php-prod -t demo-php:dev .
$ docker tag demo-php:dev {username}/demo-php:dev
$ docker push {username}/demo-php:dev
```

## References

1. Docker Build: https://docs.docker.com/engine/reference/commandline/build/ 
2. Push images to Docker Cloud: https://docs.docker.com/v17.12/docker-cloud/builds/push-images/






